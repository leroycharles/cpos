---
categories:
    - overview
date: 2024-03-25
authors: 
    - lasagnabrain
---
# Project Launch

Currently, the project is in the research phase. We are undergoing the collection of metrics or pertinent studies that will help us understand the limitations of the needs and what solutions could resolve them, along with their associated costs.