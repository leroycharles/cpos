# Blog Home page

## How to publish an article here ?

1. Create an issue with the more fitting template
2. You (or your peers) have to complete the following requirements:
    - question must have a description of maximum 250 chars;
    - all articles must have at least 1 source;
    - question must have a goal and an answer;
    - AI correction is allowed, but all AI generated text must be clearly identified;
    - answer must be at least 250 chars;
3. If you meet the requirement, you can create a merge request associated to your issue.
4. Your merge request will be accepted only if:
    - metadata for date, author, categorie and draft are set-up;
    - article have been reviewed by at least 1 maintainer.
5. Your article will be merged in the blog and published online, thus closing the related issue and MR.