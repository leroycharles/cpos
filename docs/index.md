# Home Page

## Overview

CPOS stands for "Cross Platform Orchestrator for School".

The aim is to develop a system that addresses the following issues:

- frequent compilation/testing shortens battery life (in terms of autonomy and device lifespan);
- transferring the compilation/testing problem to one or several servers requires more expensive hardware and around-the-clock maintenance;
- students might tend to have a personal laptop, a home computer, and use several cloud-based solutions, resulting in an accumulation of devices not utilized to their full potential;
- students often prioritize achieving functional code or securing the highest grade for an assignment, neglecting project management and planning ahead;
- students use generative AI as a means to an end, rather than a learning tool;
- remote and on-site development tend to create conflicts in the developing environment/tools, which induce errors;
- energy consumption is not correlated with energy production, leading to increased costs (financial and environmental).

## Blog
All ongoing research can be found in the blog.