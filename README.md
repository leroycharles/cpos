# Cross Platform Orchestrator for School

- Ongoing research can be found in [the issues](https://gitlab.com/leroycharles/cpos/-/issues)
- Research's results and additional information can be found [on the website](https://leroycharles.gitlab.io/cpos)