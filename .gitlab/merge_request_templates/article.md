Closes #x .

## Validation check list

- [ ] Description of max 250 chars
- [ ] Contains a source
- [ ] Contains a goal
- [ ] Contains an answer
- [ ] Answer is at least 250 chars
- [ ] Spell check