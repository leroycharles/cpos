## Description

To be defined.

## Goal(s)

- To be defined

## Sources

- To be defined

## Answer

To be defined.

## Validation check list

- [ ] Description of max 250 chars
- [ ] Contains a source
- [ ] Contains a goal
- [ ] Contains an answer
- [ ] Answer is at least 250 chars
- [ ] Spell check